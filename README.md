This is a repo containing debian packages to install darknet / YOLOv3 on Google Colaboratory circa December 2018.

It contains 4 packages:

1. `cudnn.tar.gz` -- based on CUDA v10.0 (v7.4.1.5 edition)
2. `vegetation-opencv-google-colab-2018_3.3.1-1ubuntu18.04.deb` -- OpenCV 3.3.1 compiled against CUDA 10.0 and the above cuDNN libraries
3. `vegetation-darknet-google-colab-2018_20181201-1ubuntu18.04.deb` -- darknet on github as of December 1, 2018 built against CUDA 10.0, and the above mentioned cuDNN and OpenCV packages.
4. `cuda-repo-ubuntu1804_10.0.130-1_amd64.deb` -- a metapackage to install CUDA v10.0 (will still need to setup the nvidia APT repo keys)

These packages are meant to be downloaded and installed in the Google Colaboratory session of interest.
